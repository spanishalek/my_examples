"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var VarietyDeleteDialogComponent = (function () {
    function VarietyDeleteDialogComponent(varietyService, activeModal, alertService, eventManager) {
        this.varietyService = varietyService;
        this.activeModal = activeModal;
        this.alertService = alertService;
        this.eventManager = eventManager;
        this.usageBlocksError = false;
    }
    VarietyDeleteDialogComponent.prototype.clear = function () {
        this.activeModal.dismiss('cancel');
    };
    VarietyDeleteDialogComponent.prototype.confirmDelete = function (id) {
        var _this = this;
        this.varietyService.delete(id).subscribe(function (response) {
            _this.eventManager.broadcast({
                name: 'varietyListModification',
                content: 'Deleted an variety'
            });
            _this.activeModal.dismiss(true);
        }, function (error) {
            if (error.headers.get('x-flowersapp-error') === 'error.Blocks') {
                _this.blocksUsageNames = error.headers.get('defaultMessage').substring(1, error.headers.get('defaultMessage').length - 1);
                _this.usageBlocksError = true;
            }
        });
        this.alertService.success('flowersApp.variety.deleted', { param: id }, null);
    };
    return VarietyDeleteDialogComponent;
}());
VarietyDeleteDialogComponent = __decorate([
    core_1.Component({
        selector: 'jhi-variety-delete-dialog',
        templateUrl: './variety-delete-dialog.component.html',
        styleUrls: ['./variety-delete-dialog.component.scss']
    })
], VarietyDeleteDialogComponent);
exports.VarietyDeleteDialogComponent = VarietyDeleteDialogComponent;
var VarietyDeletePopupComponent = (function () {
    function VarietyDeletePopupComponent(route, varietyPopupService) {
        this.route = route;
        this.varietyPopupService = varietyPopupService;
    }
    VarietyDeletePopupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeSub = this.route.params.subscribe(function (params) {
            _this.modalRef = _this.varietyPopupService
                .open(VarietyDeleteDialogComponent, 'delete-variety', params['id']);
        });
    };
    VarietyDeletePopupComponent.prototype.ngOnDestroy = function () {
        this.routeSub.unsubscribe();
    };
    return VarietyDeletePopupComponent;
}());
VarietyDeletePopupComponent = __decorate([
    core_1.Component({
        selector: 'jhi-variety-delete-popup',
        template: ''
    })
], VarietyDeletePopupComponent);
exports.VarietyDeletePopupComponent = VarietyDeletePopupComponent;
