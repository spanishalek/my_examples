"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var variety_service_1 = require("./variety.service");
var shared_1 = require("../../shared");
var forms_1 = require("@angular/forms");
var VarietyDialogComponent = (function () {
    function VarietyDialogComponent(activeModal, alertService, varietyService, eventManager, rd, fb) {
        this.activeModal = activeModal;
        this.alertService = alertService;
        this.varietyService = varietyService;
        this.eventManager = eventManager;
        this.rd = rd;
        this.fb = fb;
        this.files = new Array();
        this.nameMax = 35;
        this.colorMax = 25;
        this.breederMax = 25;
        this.digitMax = 4;
        this.createForm();
        this.uploadEvent = new core_1.EventEmitter();
    }
    VarietyDialogComponent.prototype.createForm = function () {
        this.formGroup = this.fb.group({
            typeOfFlowers: new forms_1.FormControl()
        });
    };
    VarietyDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.variety.id) {
            this.varietyService.getBase64FileById(this.variety.id).subscribe(function (response) {
                _this.base64Image = response[0];
            });
        }
        else {
            this.varietyService.getDefaultImage().subscribe(function (response) {
                _this.base64Image = response[0];
            });
        }
        if (!this.typeOfFlowerId) {
            this.fillTypeOfFlowers();
        }
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.tmpName = '';
        this.registerEvents();
    };
    VarietyDialogComponent.prototype.ngOnDestroy = function () {
        this.eventManager.destroy(this.deletedImageEvent);
        this.eventManager.destroy(this.newDeletedImageEvent);
    };
    VarietyDialogComponent.prototype.registerEvents = function () {
        var _this = this;
        this.deletedImageEvent = this.eventManager.subscribe('varietyImageDeleted', function (response) { return _this.resetDefaultImage(); });
        this.newDeletedImageEvent = this.eventManager.subscribe('newVarietyImageDeleted', function (response) { return _this.resetDefaultImage(); });
    };
    VarietyDialogComponent.prototype.resetDefaultImage = function () {
        var _this = this;
        this.varietyService.getDefaultImage().subscribe(function (response) {
            _this.base64Image = response[0];
            _this.image.nativeElement.src = 'data:image/png;base64,' + _this.base64Image;
            _this.inputEl.nativeElement.value = '';
        });
    };
    VarietyDialogComponent.prototype.clear = function () {
        this.activeModal.dismiss('cancel');
    };
    VarietyDialogComponent.prototype.save = function () {
        this.isSaving = true;
        if (!this.variety.breeder) {
            this.variety.breeder = null;
        }
        if (!this.variety.minLength) {
            this.variety.minLength = null;
        }
        if (!this.variety.maxLength) {
            this.variety.maxLength = null;
        }
        if (!this.variety.typeOfFlower) {
            this.variety.typeOfFlower = this.typeOfFlowers[0];
        }
        if (!this.variety.id) {
            this.subscribeToSaveResponse(this.varietyService.create(this.variety), true);
        }
        else {
            this.subscribeToSaveResponse(this.varietyService.update(this.variety), false);
        }
    };
    VarietyDialogComponent.prototype.subscribeToSaveResponse = function (result, isCreated) {
        var _this = this;
        result.subscribe(function (res) {
            return _this.onSaveSuccess(res, isCreated);
        }, function (res) { return _this.onSaveError(res); });
    };
    VarietyDialogComponent.prototype.onSaveSuccess = function (result, isCreated) {
        this.alertService.success(isCreated ? 'flowersApp.variety.created'
            : 'flowersApp.variety.updated', { param: result.id }, null);
        this.variety.id = result.id;
        this.sendImage(this.variety.id);
        this.eventManager.broadcast({ name: 'varietyListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    };
    VarietyDialogComponent.prototype.onSaveError = function (error) {
        try {
            error.json();
        }
        catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    };
    VarietyDialogComponent.prototype.onError = function (error) {
        if (error.headers.get('x-flowersapp-error') === 'error.DuplicateName') {
            this.tmpName = this.variety.name.toLowerCase().trim();
        }
        else {
            this.alertService.error(error.message, null, null);
        }
    };
    VarietyDialogComponent.prototype.fillTypeOfFlowerNames = function () {
        this.typeOfFlowerNames = this.typeOfFlowers.map(function (t) { return t.name; });
    };
    VarietyDialogComponent.prototype.fillTypeOfFlowers = function () {
        var _this = this;
        this.varietyService.findAllTypeOfFlowers().subscribe(function (typeOfFlowers) {
            _this.typeOfFlowers = typeOfFlowers;
            _this.fillTypeOfFlowerNames();
            _this.initFormValues();
        });
    };
    VarietyDialogComponent.prototype.initFormValues = function () {
        this.setTypeOfFlowersValue(this.formGroup.get('typeOfFlowers'));
    };
    VarietyDialogComponent.prototype.setTypeOfFlowersValue = function (control) {
        var names = this.typeOfFlowers.map(function (t) { return t.name; });
        var id = this.typeOfFlowers.map(function (t) { return t.id; });
        if (names && names.length && names[0]) {
            if (!this.variety.typeOfFlower) {
                var text = names[0];
                control.setValue([{ id: this.typeOfFlowers[0].name, text: text }]);
                this.typeOfFlowerId = id[0];
            }
            else {
                var text = this.variety.typeOfFlower.name;
                control.setValue([{ id: this.variety.typeOfFlower.name, text: text }]);
                this.typeOfFlowerId = this.variety.typeOfFlower.id;
            }
        }
    };
    VarietyDialogComponent.prototype.selectedTypeOfFlower = function (value) {
        var _this = this;
        this.typeOfFlowers.forEach(function (t) {
            if (t.name === value.text) {
                _this.variety.typeOfFlower = t;
            }
        });
    };
    VarietyDialogComponent.prototype.sendImage = function (varietyId) {
        var _this = this;
        var fileName;
        if (!this.isSaving) {
            var ext = this.imageFileName.substring(this.imageFileName.lastIndexOf('.'), this.imageFileName.length);
            fileName = varietyId.toString() + ext;
        }
        else {
            fileName = varietyId.toString() + this.extension;
        }
        if (this.file) {
            this.varietyService.makeFileRequest(this.file, fileName).subscribe(function (response) {
                _this.eventManager.broadcast({ name: 'imageUploaded' });
            });
        }
    };
    VarietyDialogComponent.prototype.upload = function () {
        this.htmlInputEl = this.inputEl.nativeElement;
        var imageFile = this.htmlInputEl.files.item(0);
        this.readModalFiles(this.image.nativeElement, imageFile);
        this.readFiles(imageFile);
    };
    VarietyDialogComponent.prototype.readFile = function (file, reader, callback) {
        reader.onload = function () {
            callback(reader.result);
        };
        reader.readAsDataURL(file);
    };
    VarietyDialogComponent.prototype.readFiles = function (file) {
        var _this = this;
        this.extension = file.name.substring(file.name.lastIndexOf('.'), file.name.length);
        var reader = new FileReader();
        this.readFile(file, reader, function (result) {
            var img = document.createElement('img');
            img.src = result;
            _this.resize(img, file.name, 189, 120, function (resized_image) {
                _this.file = resized_image;
                _this.resized_image = resized_image;
                _this.uploadEvent.emit(true);
            });
        });
    };
    VarietyDialogComponent.prototype.readModalFiles = function (image, file) {
        var _this = this;
        var reader = new FileReader();
        this.readFile(file, reader, function (result) {
            var img = document.createElement('img');
            img.src = result;
            _this.resize(img, file.name, 189, 120, function (resized_image) {
                _this.rd.setAttribute(image, 'src', resized_image);
            });
        });
    };
    VarietyDialogComponent.prototype.resize = function (img, fileName, MAX_WIDTH, MAX_HEIGHT, callback) {
        var _this = this;
        return img.onload = function () {
            var width = img.width;
            var height = img.height;
            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            }
            else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }
            var canvas = document.createElement('canvas');
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0, width, height);
            var pngExtension = ['png'];
            var jpgExtension = ['jpg'];
            var jpegExtension = ['jpeg'];
            var tempFileName = fileName;
            var imageExtension = fileName.split('.').pop();
            var dataUrl;
            if (_this.isInArray(pngExtension, imageExtension)) {
                if (_this.variety && _this.variety.id) {
                    _this.imageFileName = tempFileName.replace(fileName, _this.variety.id.toString().concat('.').concat(pngExtension[0]));
                }
                dataUrl = canvas.toDataURL('image/png');
            }
            if (_this.isInArray(jpgExtension, imageExtension)) {
                if (_this.variety && _this.variety.id) {
                    _this.imageFileName = tempFileName.replace(fileName, _this.variety.id.toString().concat('.').concat(jpgExtension[0]));
                }
                dataUrl = canvas.toDataURL('image/jpg');
            }
            if (_this.isInArray(jpegExtension, imageExtension)) {
                if (_this.variety && _this.variety.id) {
                    _this.imageFileName = tempFileName.replace(fileName, _this.variety.id.toString().concat('.').concat(jpegExtension[0]));
                }
                dataUrl = canvas.toDataURL('image/jpeg');
            }
            callback(dataUrl, img.src.length, dataUrl.length, dataUrl.name);
        };
    };
    /*- checks if extension exists in array -*/
    VarietyDialogComponent.prototype.isInArray = function (array, word) {
        return array.indexOf(word.toLowerCase()) > -1;
    };
    VarietyDialogComponent.prototype.requiredValidation = function (data) {
        return !data || !this.isFillValidation(data);
    };
    VarietyDialogComponent.prototype.isFillValidation = function (fieldData) {
        return fieldData && fieldData.toString().trim();
    };
    VarietyDialogComponent.prototype.digitValidation = function (varietyLength) {
        if (varietyLength && varietyLength.toString()) {
            return !varietyLength.toString().match('') && !varietyLength.toString().match(shared_1.DIGITS);
        }
    };
    VarietyDialogComponent.prototype.latinValidation = function (data, maxLength) {
        if (data && !this.maxLength(data, maxLength)) {
            return !data.match(shared_1.LATIN_VALIDATION);
        }
    };
    VarietyDialogComponent.prototype.maxLength = function (data, maxLength) {
        if (data && data.toString().length) {
            return data.toString().length > maxLength;
        }
    };
    VarietyDialogComponent.prototype.duplicateValidation = function (fieldData) {
        return this.variety.name && this.tmpName && this.tmpName === this.variety.name.toLowerCase().trim() && !this.maxLength(fieldData, this.nameMax);
    };
    VarietyDialogComponent.prototype.saveButtonDeactivation = function (variety) {
        return this.requiredValidation(variety.name) || this.requiredValidation(variety.color)
            || this.digitValidation(variety.minLength) || this.digitValidation(variety.maxLength)
            || this.latinValidation(variety.name, this.nameMax) || this.latinValidation(variety.color, this.colorMax)
            || this.latinValidation(variety.breeder, this.breederMax) || this.maxLength(variety.name, this.nameMax)
            || this.maxLength(variety.color, this.colorMax) || this.maxLength(variety.breeder, this.breederMax)
            || this.maxLength(variety.minLength, this.digitMax) || this.maxLength(variety.maxLength, this.digitMax);
    };
    return VarietyDialogComponent;
}());
__decorate([
    core_1.ViewChild('fileInput')
], VarietyDialogComponent.prototype, "inputEl", void 0);
__decorate([
    core_1.ViewChild('image')
], VarietyDialogComponent.prototype, "image", void 0);
__decorate([
    core_1.Output()
], VarietyDialogComponent.prototype, "uploadEvent", void 0);
VarietyDialogComponent = __decorate([
    core_1.Component({
        selector: 'jhi-variety-dialog',
        templateUrl: './variety-dialog.component.html',
        styleUrls: ['./variety-dialog.component.scss'],
        providers: [variety_service_1.VarietyService]
    })
], VarietyDialogComponent);
exports.VarietyDialogComponent = VarietyDialogComponent;
var VarietyPopupComponent = (function () {
    function VarietyPopupComponent(route, varietyPopupService) {
        this.route = route;
        this.varietyPopupService = varietyPopupService;
    }
    VarietyPopupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeSub = this.route.params.subscribe(function (params) {
            if (params['id']) {
                _this.modalRef = _this.varietyPopupService
                    .open(VarietyDialogComponent, 'variety-list-modal-window', params['id']);
            }
            else {
                _this.modalRef = _this.varietyPopupService
                    .open(VarietyDialogComponent, 'variety-list-modal-window');
            }
        });
    };
    VarietyPopupComponent.prototype.ngOnDestroy = function () {
        this.routeSub.unsubscribe();
    };
    return VarietyPopupComponent;
}());
VarietyPopupComponent = __decorate([
    core_1.Component({
        selector: 'jhi-variety-popup',
        template: ''
    })
], VarietyPopupComponent);
exports.VarietyPopupComponent = VarietyPopupComponent;
