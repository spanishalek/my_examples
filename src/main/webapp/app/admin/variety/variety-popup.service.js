"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var variety_model_1 = require("../../entities/variety/variety.model");
var VarietyPopupService = (function () {
    function VarietyPopupService(modalService, router, varietyService) {
        this.modalService = modalService;
        this.router = router;
        this.varietyService = varietyService;
        this.isOpen = false;
    }
    VarietyPopupService.prototype.open = function (component, windowClass, id) {
        var _this = this;
        this.isOpen = true;
        if (id) {
            this.varietyService.find(id).subscribe(function (variety) {
                _this.varietyModalRef(component, variety, windowClass);
            });
        }
        else {
            return this.varietyModalRef(component, new variety_model_1.Variety(), windowClass);
        }
    };
    VarietyPopupService.prototype.varietyModalRef = function (component, variety, windowClass) {
        var _this = this;
        var modalRef = this.modalService.open(component, { windowClass: windowClass });
        modalRef.componentInstance.variety = variety;
        modalRef.result.then(function (result) {
            _this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true });
            _this.isOpen = false;
        }, function (reason) {
            _this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true });
            _this.isOpen = false;
        });
        return modalRef;
    };
    return VarietyPopupService;
}());
VarietyPopupService = __decorate([
    core_1.Injectable()
], VarietyPopupService);
exports.VarietyPopupService = VarietyPopupService;
