"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by aleksey on 27.06.17.
 */
var core_1 = require("@angular/core");
var VarietyDeleteImageComponent = (function () {
    function VarietyDeleteImageComponent(varietyService, activeModal, alertService, eventManager) {
        this.varietyService = varietyService;
        this.activeModal = activeModal;
        this.alertService = alertService;
        this.eventManager = eventManager;
    }
    VarietyDeleteImageComponent.prototype.clear = function () {
        this.activeModal.dismiss('cancel');
    };
    VarietyDeleteImageComponent.prototype.confirmImageDelete = function (id) {
        var _this = this;
        if (id) {
            this.varietyService.deleteImage(id).subscribe(function (response) {
                _this.eventManager.broadcast({
                    name: 'varietyImageDeleted',
                    content: 'Deleted an variety'
                });
                _this.activeModal.dismiss(true);
            });
            this.alertService.success('flowersApp.variety.deleted', { param: id }, null);
        }
        else {
            this.eventManager.broadcast({
                name: 'newVarietyImageDeleted',
                content: 'Deleted an variety'
            });
            this.activeModal.dismiss(true);
        }
    };
    return VarietyDeleteImageComponent;
}());
VarietyDeleteImageComponent = __decorate([
    core_1.Component({
        selector: 'jhi-variety-delete-image',
        templateUrl: './variety-delete-image.component.html',
        styleUrls: ['./variety-delete-dialog.component.scss']
    })
], VarietyDeleteImageComponent);
exports.VarietyDeleteImageComponent = VarietyDeleteImageComponent;
var ImageDeletePopupComponent = (function () {
    function ImageDeletePopupComponent(route, varietyPopupService) {
        this.route = route;
        this.varietyPopupService = varietyPopupService;
    }
    ImageDeletePopupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeSub = this.route.params.subscribe(function (params) {
            _this.modalRef = _this.varietyPopupService
                .open(VarietyDeleteImageComponent, 'delete-image', params['id']);
        });
    };
    ImageDeletePopupComponent.prototype.ngOnDestroy = function () {
        this.routeSub.unsubscribe();
    };
    return ImageDeletePopupComponent;
}());
ImageDeletePopupComponent = __decorate([
    core_1.Component({
        selector: 'jhi-variety-delete-popup',
        template: ''
    })
], ImageDeletePopupComponent);
exports.ImageDeletePopupComponent = ImageDeletePopupComponent;
