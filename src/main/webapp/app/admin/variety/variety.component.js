"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var VarietyComponent = (function () {
    function VarietyComponent(varietyService, alertService, eventManager, principal, fb) {
        this.varietyService = varietyService;
        this.alertService = alertService;
        this.eventManager = eventManager;
        this.principal = principal;
        this.fb = fb;
        this.varieties = [];
        this.page = 0;
        this.predicate = 'id';
        this.reverse = true;
        this.createForm();
    }
    VarietyComponent.prototype.createForm = function () {
        this.formGroup = this.fb.group({
            typeOfFlowers: new forms_1.FormControl()
        });
    };
    VarietyComponent.prototype.loadAll = function () {
        var _this = this;
        this.varietyService.getVarietiesByTypeOfFlowers(this.typeOfFlowerId).subscribe(function (varieties) { return _this.onSuccess(varieties, _this.typeOfFlowerId); }, function (varieties) { return _this.onError(varieties); });
    };
    VarietyComponent.prototype.reset = function () {
        this.page = 0;
        this.varieties = [];
        this.loadAll();
    };
    VarietyComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.typeOfFlowerId) {
            this.fillTypeOfFlowers();
        }
        else {
            this.loadAll();
        }
        this.principal.identity().then(function (account) {
            _this.currentAccount = account;
        });
        this.registerChangeInVarieties();
    };
    VarietyComponent.prototype.ngOnDestroy = function () {
        this.eventManager.destroy(this.eventSubscriber);
        this.eventManager.destroy(this.deletedImageEvent);
        this.eventManager.destroy(this.imageEventSubscriber);
        this.eventManager.destroy(this.typeOfFlowersSubscriber);
    };
    VarietyComponent.prototype.trackId = function (index, item) {
        return item.id;
    };
    VarietyComponent.prototype.registerChangeInVarieties = function () {
        var _this = this;
        this.eventSubscriber = this.eventManager.subscribe('varietyListModification', function (response) { return _this.reset(); });
        this.imageEventSubscriber = this.eventManager.subscribe('imageUploaded', function (response) { return _this.reset(); });
        this.deletedImageEvent = this.eventManager.subscribe('varietyImageDeleted', function (response) { return _this.reset(); });
        this.typeOfFlowersSubscriber = this.eventManager.subscribe('changedTypeOfFlowers', function (response) { return _this.reset(); });
    };
    VarietyComponent.prototype.sort = function () {
        var result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    };
    VarietyComponent.prototype.onSuccess = function (data, typeOfFlowersId) {
        var _this = this;
        this.varieties = data;
        this.varietyService.getAllBase64FilesByCurrentCompanyAndTypeOfFlower(typeOfFlowersId).subscribe(function (response) {
            _this.base64Images = [];
            _this.base64Images = response;
        });
    };
    VarietyComponent.prototype.onError = function (error) {
        this.alertService.error(error.message, null, null);
    };
    VarietyComponent.prototype.onValue = function (fieldData) {
        if (fieldData) {
            return fieldData.toString().toLowerCase();
        }
        else {
            return '---';
        }
    };
    VarietyComponent.prototype.fillTypeOfFlowerNames = function () {
        this.typeOfFlowerNames = this.typeOfFlowers.map(function (t) { return t.name; });
    };
    VarietyComponent.prototype.fillTypeOfFlowers = function () {
        var _this = this;
        this.varietyService.findAllTypeOfFlowers().subscribe(function (typeOfFlowers) {
            _this.typeOfFlowers = typeOfFlowers;
            _this.fillTypeOfFlowerNames();
            _this.initFormValues();
        });
    };
    VarietyComponent.prototype.initFormValues = function () {
        this.setTypeOfFlowersValue(this.formGroup.get('typeOfFlowers'));
    };
    VarietyComponent.prototype.setTypeOfFlowersValue = function (control) {
        var names = this.typeOfFlowers.map(function (t) { return t.name; });
        var id = this.typeOfFlowers.map(function (t) { return t.id; });
        if (names && names.length && names[0]) {
            var text = names[0];
            control.setValue([{ id: this.typeOfFlowers[0].name, text: text }]);
            this.typeOfFlowerId = id[0];
            this.loadAll();
        }
    };
    VarietyComponent.prototype.selectedTypeOfFlower = function (value) {
        var _this = this;
        console.log(value.text);
        this.typeOfFlowers.forEach(function (t) {
            if (t.name === value.text) {
                _this.typeOfFlowerId = t.id;
                _this.eventManager.broadcast({ name: 'changedTypeOfFlowers' });
            }
        });
    };
    return VarietyComponent;
}());
__decorate([
    core_1.ViewChild('image')
], VarietyComponent.prototype, "image", void 0);
VarietyComponent = __decorate([
    core_1.Component({
        selector: 'jhi-variety',
        templateUrl: './variety.component.html',
        styleUrls: ['./variety.component.scss'],
    })
], VarietyComponent);
exports.VarietyComponent = VarietyComponent;
