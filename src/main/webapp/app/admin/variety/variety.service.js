"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
var shared_1 = require("../../shared");
var response_util_1 = require("../../util/response-util");
var VarietyService = (function () {
    function VarietyService(http) {
        this.http = http;
        this.resourceUrl = 'api/varieties';
        this.typeOfFlowerUrl = 'api/type-of-flowers';
    }
    VarietyService.prototype.create = function (variety) {
        var copy = this.convert(variety);
        return this.http.post(this.resourceUrl, copy).map(function (res) {
            return res.json();
        });
    };
    VarietyService.prototype.update = function (variety) {
        var copy = this.convert(variety);
        return this.http.put(this.resourceUrl, copy).map(function (res) {
            return res.json();
        });
    };
    VarietyService.prototype.find = function (id) {
        return this.http.get(this.resourceUrl + "/" + id).map(function (res) {
            return res.json();
        });
    };
    VarietyService.prototype.query = function (req) {
        var _this = this;
        var options = shared_1.createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map(function (res) { return _this.convertResponse(res); });
    };
    VarietyService.prototype.delete = function (id) {
        return this.http.delete(this.resourceUrl + "/" + id)
            .map(function (res) {
            return response_util_1.default.toJson(res);
        })
            .catch(function (error) {
            return Rx_1.Observable.throw(error);
        });
    };
    VarietyService.prototype.deleteImage = function (id) {
        return this.http.delete(this.resourceUrl + "/" + id + "/image");
    };
    VarietyService.prototype.convertResponse = function (res) {
        var jsonResponse = res.json();
        return new shared_1.ResponseWrapper(res.headers, jsonResponse, res.status);
    };
    VarietyService.prototype.convert = function (variety) {
        var copy = Object.assign({}, variety);
        return copy;
    };
    VarietyService.prototype.makeFileRequest = function (file, fileName) {
        return this.http.post(this.resourceUrl + '/uploadVarietyImage', JSON.stringify({ base64file: file, name: fileName }), {
            headers: new http_1.Headers({ 'Content-Type': 'application/json' })
        });
    };
    VarietyService.prototype.findAllTypeOfFlowers = function () {
        return this.http.get(this.typeOfFlowerUrl + '/company').map(function (res) {
            return res.json();
        });
    };
    VarietyService.prototype.getVarietiesByTypeOfFlowers = function (typeOfFlowerId) {
        return this.http.get(this.resourceUrl + "/type-of-flower/" + typeOfFlowerId).map(function (res) {
            return res.json();
        });
    };
    VarietyService.prototype.getAllBase64FilesByCurrentCompanyAndTypeOfFlower = function (typeOfFlowersId) {
        return this.http.get(this.resourceUrl + "/type-of-flower-images/" + typeOfFlowersId).map(function (res) {
            return res.json();
        });
    };
    VarietyService.prototype.getBase64FileById = function (varietyId) {
        return this.http.get(this.resourceUrl + "/image/" + varietyId).map(function (res) {
            return res.json();
        });
    };
    VarietyService.prototype.getDefaultImage = function () {
        return this.http.get(this.resourceUrl + '/default-image').map(function (res) {
            return res.json();
        });
    };
    return VarietyService;
}());
VarietyService = __decorate([
    core_1.Injectable()
], VarietyService);
exports.VarietyService = VarietyService;
