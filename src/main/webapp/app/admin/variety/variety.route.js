"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var shared_1 = require("../../shared");
var variety_component_1 = require("./variety.component");
var variety_dialog_component_1 = require("./variety-dialog.component");
var variety_delete_dialog_component_1 = require("./variety-delete-dialog.component");
var variety_delete_image_component_1 = require("./variety-delete-image.component");
exports.varietyRoute = [
    {
        path: 'variety',
        component: variety_component_1.VarietyComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'flowersApp.variety.home.title'
        },
        canActivate: [shared_1.UserRouteAccessService]
    }
];
exports.varietyPopupRoute = [
    {
        path: 'variety-new',
        component: variety_dialog_component_1.VarietyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'flowersApp.variety.home.title'
        },
        canActivate: [shared_1.UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'variety/:id/edit',
        component: variety_dialog_component_1.VarietyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'flowersApp.variety.home.title'
        },
        canActivate: [shared_1.UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'variety/:id/image/delete',
        component: variety_delete_image_component_1.ImageDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [shared_1.UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'variety/image/delete',
        component: variety_delete_image_component_1.ImageDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
        },
        canActivate: [shared_1.UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'variety/:id/delete',
        component: variety_delete_dialog_component_1.VarietyDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'flowersApp.variety.home.title'
        },
        canActivate: [shared_1.UserRouteAccessService],
        outlet: 'popup'
    },
];
