"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var shared_1 = require("../../shared");
var variety_delete_image_component_1 = require("./variety-delete-image.component");
var common_1 = require("@angular/common");
var variety_route_1 = require("./variety.route");
var variety_component_1 = require("./variety.component");
var variety_dialog_component_1 = require("./variety-dialog.component");
var variety_delete_dialog_component_1 = require("./variety-delete-dialog.component");
var variety_service_1 = require("./variety.service");
var variety_popup_service_1 = require("./variety-popup.service");
var ng2_select_1 = require("ng2-select");
var forms_1 = require("@angular/forms");
var ENTITY_STATES = variety_route_1.varietyRoute.concat(variety_route_1.varietyPopupRoute);
var FlowersVarietyModule = (function () {
    function FlowersVarietyModule() {
    }
    return FlowersVarietyModule;
}());
FlowersVarietyModule = __decorate([
    core_1.NgModule({
        imports: [
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            common_1.CommonModule,
            shared_1.FlowersSharedModule,
            ng2_select_1.SelectModule,
            router_1.RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        ],
        declarations: [
            variety_component_1.VarietyComponent,
            variety_dialog_component_1.VarietyDialogComponent,
            variety_delete_dialog_component_1.VarietyDeleteDialogComponent,
            variety_dialog_component_1.VarietyPopupComponent,
            variety_delete_dialog_component_1.VarietyDeletePopupComponent,
            variety_delete_image_component_1.VarietyDeleteImageComponent,
            variety_delete_image_component_1.ImageDeletePopupComponent,
        ],
        entryComponents: [
            variety_component_1.VarietyComponent,
            variety_dialog_component_1.VarietyDialogComponent,
            variety_dialog_component_1.VarietyPopupComponent,
            variety_delete_dialog_component_1.VarietyDeleteDialogComponent,
            variety_delete_dialog_component_1.VarietyDeletePopupComponent,
            variety_delete_image_component_1.VarietyDeleteImageComponent,
            variety_delete_image_component_1.ImageDeletePopupComponent,
        ],
        providers: [
            variety_service_1.VarietyService,
            variety_popup_service_1.VarietyPopupService,
        ],
        exports: [variety_component_1.VarietyComponent],
        schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
    })
], FlowersVarietyModule);
exports.FlowersVarietyModule = FlowersVarietyModule;
